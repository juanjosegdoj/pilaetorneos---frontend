import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TorneoEditarComponent } from './torneo-editar.component';

describe('TorneoEditarComponent', () => {
  let component: TorneoEditarComponent;
  let fixture: ComponentFixture<TorneoEditarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TorneoEditarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TorneoEditarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
