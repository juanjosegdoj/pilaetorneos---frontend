import { Component, OnInit, Input, OnChanges} from '@angular/core';
import { Torneo } from 'src/app/model/torneo';
import { Deporte } from 'src/app/model/deporte';
import { DeporteService } from 'src/app/services/deporte.service';
import { Genero } from 'src/app/model/genero';
import { GeneroService } from 'src/app/services/genero.service';
import { TorneoService } from 'src/app/services/torneo.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-torneo-editar',
  templateUrl: './torneo-editar.component.html',
  styleUrls: ['./torneo-editar.component.css']
})
export class TorneoEditarComponent implements OnInit, OnChanges {

  deportes: Deporte[] = [];
  generos: Genero[] = [];
  torneo: Torneo = new Torneo();

  constructor(private deporteService: DeporteService, private generoService: GeneroService,
    private torneoService: TorneoService, private route: ActivatedRoute,
    private router: Router
    ) {
  }

  // Este es el encargado de que se refresquen los campos de deporte y género
  ngOnChanges(): void {
    this.torneoService.findById(this.route.snapshot.paramMap.get('id')).subscribe(
      torneo => {
        this.torneo = torneo;
      }
    );
  }

  ngOnInit() {
    this.torneoService.findById(this.route.snapshot.paramMap.get('id')).subscribe(
      torneo => {
        this.torneo = torneo;
      }
    );

    // Obteniendo todos los deportes
    this.deporteService.listAll().subscribe(
      (deportes) => {
        this.deportes = deportes;
      }
    );
    // Obteniendo todos los géneros
    this.generoService.listAll().subscribe(
      (generos) => {
        this.generos = generos;
      }
    );
  }

  onClickMeGenero(genero: Genero)   {
    this.torneo.genero = genero;
  }

  editarTorneo() {
    this.torneoService.editarTorneo(this.torneo).subscribe(
      _ => this.router.navigate(['/torneos'])
    );
  }

}
