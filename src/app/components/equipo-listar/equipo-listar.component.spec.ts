import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EquipoListarComponent } from './equipo-listar.component';

describe('EquipoListarComponent', () => {
  let component: EquipoListarComponent;
  let fixture: ComponentFixture<EquipoListarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EquipoListarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EquipoListarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
