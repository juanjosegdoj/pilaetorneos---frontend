import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { EquipoService } from 'src/app/services/equipo.service';
import { Equipo } from 'src/app/model/equipo';
import { Jugador } from 'src/app/model/jugador';
import { Torneo } from 'src/app/model/torneo';
import { TorneoService } from 'src/app/services/torneo.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-equipo-listar',
  templateUrl: './equipo-listar.component.html',
  styleUrls: ['./equipo-listar.component.css']
})
export class EquipoListarComponent implements OnInit {

  private torneoSeleccionado: Torneo;
  private equipos: Equipo[] = [];
  private jugadores: Jugador[] = [];

  constructor(private equipoService: EquipoService, private torneoService: TorneoService,
    private route: ActivatedRoute, private router: Router, private modalService: NgbModal) { }

  ngOnInit() {

    this.torneoService.findById(this.route.snapshot.paramMap.get('id')).subscribe(
      torneo => {
        this.torneoSeleccionado = torneo;
      }
    );

    this.equipoService.findEquiposByTorneo(this.route.snapshot.paramMap.get('id')).subscribe(
      equipos => {
        this.equipos = equipos;
        this.equipos.forEach(equipo => {
          this.jugadores = this.jugadores.concat(equipo.jugadores);
        });
      }
    );
  }

  openModalCrear(content: any) {
    this.modalService.open(content, { size: 'lg' });
  }

  selectEquipo(id_equipo: number): void {
    this.router.navigate(['/torneo', this.torneoSeleccionado.codigo, 'equipo', id_equipo]);
  }
}
