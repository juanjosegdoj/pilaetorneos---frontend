import { Component, OnInit } from '@angular/core';
import { Deporte } from 'src/app/model/deporte';
import { DeporteService } from 'src/app/services/deporte.service';

@Component({
  selector: 'app-deporte',
  templateUrl: './deporte.component.html',
  styleUrls: ['./deporte.component.css']
})
export class DeporteComponent implements OnInit {

  deportes: Deporte[] = [];
  deporteSeleccionado = new Deporte();
  constructor(private deporteService: DeporteService) { }

  ngOnInit() {
    this.deporteService.listAll().subscribe(
      (deportes) => {
        this.deportes = deportes;
      }
    );
  }

  showPopup(deporte: Deporte) {
    this.deporteSeleccionado = deporte;
  }

}
