import { Component, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { TorneoService } from 'src/app/services/torneo.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-torneo-configuracion',
  templateUrl: './torneo-configuracion.component.html',
  styleUrls: ['./torneo-configuracion.component.css']
})
export class TorneoConfiguracionComponent implements OnInit {

  constructor(private route: ActivatedRoute, private torneoService: TorneoService,
    private modalService: NgbModal, private router: Router) { }

  ngOnInit() {
  }

  openModalEliminar(content: any) {
    this.modalService.open(content, { size: 'lg' });
  }

  eliminarTorneo() {
    this.torneoService.deleteTorneo(this.route.snapshot.paramMap.get('id')).subscribe(
      _ => this.router.navigate(['torneos'])
      );
  }
}
