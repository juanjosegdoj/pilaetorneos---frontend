import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TorneoConfiguracionComponent } from './torneo-configuracion.component';

describe('TorneoConfiguracionComponent', () => {
  let component: TorneoConfiguracionComponent;
  let fixture: ComponentFixture<TorneoConfiguracionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TorneoConfiguracionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TorneoConfiguracionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
