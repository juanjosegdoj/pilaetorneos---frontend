import { Component, OnInit, Input } from '@angular/core';
import { Equipo } from 'src/app/model/equipo';
import { EquipoService } from 'src/app/services/equipo.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-equipo-registrar',
  templateUrl: './equipo-registrar.component.html',
  styleUrls: ['./equipo-registrar.component.css']
})
export class EquipoRegistrarComponent implements OnInit {

  private equipo: Equipo = new Equipo();
  @Input() modal: any; // para cerrar el modal creado en equipo.component
  @Input() codTorneo: number; // para cerrar el modal creado en equipo.component

  constructor(private equipoService: EquipoService, private router: Router) { }

  ngOnInit() {
  }

  saveEquipo() {
    this.modal.close('Close click');
    this.equipoService.insertarEquipoId(this.codTorneo.toString(), this.equipo).subscribe(
      _ => this.router.navigate(['/torneo', this.codTorneo.toString(), 'equipos'])
    );
    console.log('Necesito que la página que lancé se refresque');
  }
}
