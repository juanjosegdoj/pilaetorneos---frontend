import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EquipoRegistrarComponent } from './equipo-registrar.component';

describe('EquipoRegistrarComponent', () => {
  let component: EquipoRegistrarComponent;
  let fixture: ComponentFixture<EquipoRegistrarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EquipoRegistrarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EquipoRegistrarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
