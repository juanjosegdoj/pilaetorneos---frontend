import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TorneoRegistroComponent } from './torneo-registro.component';

describe('TorneoRegistroComponent', () => {
  let component: TorneoRegistroComponent;
  let fixture: ComponentFixture<TorneoRegistroComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TorneoRegistroComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TorneoRegistroComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
