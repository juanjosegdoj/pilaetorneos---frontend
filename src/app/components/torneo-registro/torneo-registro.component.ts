import { Component, OnInit, Input} from '@angular/core';
import { Torneo } from 'src/app/model/torneo';
import { Deporte } from 'src/app/model/deporte';
import { DeporteService } from 'src/app/services/deporte.service';
import { Genero } from 'src/app/model/genero';
import { GeneroService } from 'src/app/services/genero.service';
import { TorneoService } from 'src/app/services/torneo.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-torneo-registro',
  templateUrl: './torneo-registro.component.html',
  styleUrls: ['./torneo-registro.component.css']
})
export class TorneoRegistroComponent implements OnInit {

  torneo: Torneo;
  deportes: Deporte[] = [];
  generos: Genero[] = [];
  @Input() modal: any; // para cerrar el modal creado en torneo.component

  constructor(private deporteService: DeporteService, private generoService: GeneroService,
    private torneoService: TorneoService, private router: Router) {
    this.torneo = new Torneo();
  }

  ngOnInit() {
    // Obteniendo todos los deportes
    this.deporteService.listAll().subscribe(
      (deportes) => {
        this.deportes = deportes;
      }
    );
    // Obteniendo todos los géneros
    this.generoService.listAll().subscribe(
      (generos) => {
        this.generos = generos;
      }
    );
  }

  onClickMeGenero(genero: Genero)   {
    this.torneo.genero = genero;
  }


  saveTorneo() {
    this.torneoService.createTorneo(this.torneo).subscribe(
      _ => this.modal.close('Close click')
    );
  }
}
