import { Component, OnInit, DoCheck, OnChanges } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { TorneoService } from 'src/app/services/torneo.service';
import { Torneo } from 'src/app/model/torneo';

@Component({
  selector: 'app-panel-torneo',
  templateUrl: './panel-torneo.component.html',
  styleUrls: ['./panel-torneo.component.css']
})
export class PanelTorneoComponent implements OnInit {

  torneos: Torneo[];
  torneoSeleccionado: Torneo = new Torneo();

  constructor(private router: Router, private route: ActivatedRoute, private torneoService: TorneoService) {
    this.torneoSeleccionado = new Torneo();
   }

  ngOnInit() {
    const id: string = this.route.firstChild.snapshot.paramMap.get('id');

    this.torneoService.listAll().subscribe(
      (torneos) => {
        this.torneos = torneos;
        this.torneoSeleccionado = torneos.filter(x => x.codigo.toString() === id)[0];
      }
    );
  }


  seleccionarOtroTorneo() {
    // EL siguiente es un machetazo para lograr refrescar el componente
    this.router.navigateByUrl('', { skipLocationChange: true }).then(
      _ => this.router.navigate(['/torneo', this.torneoSeleccionado.codigo, 'equipos'])
      );
  }
}
