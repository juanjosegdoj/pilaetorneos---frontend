import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PanelTorneoComponent } from './panel-torneo.component';

describe('PanelTorneoComponent', () => {
  let component: PanelTorneoComponent;
  let fixture: ComponentFixture<PanelTorneoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PanelTorneoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PanelTorneoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
