import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { JugadorRegistroComponent } from './jugador-registro.component';

describe('JugadorRegistroComponent', () => {
  let component: JugadorRegistroComponent;
  let fixture: ComponentFixture<JugadorRegistroComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ JugadorRegistroComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(JugadorRegistroComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
