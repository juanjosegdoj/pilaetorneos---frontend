import { Component, OnInit, Input } from '@angular/core';
import { Jugador } from 'src/app/model/jugador';
import { Equipo } from 'src/app/model/equipo';
import { JugadorService } from 'src/app/services/jugador.service';

@Component({
  selector: 'app-jugador-registro',
  templateUrl: './jugador-registro.component.html',
  styleUrls: ['./jugador-registro.component.css']
})
export class JugadorRegistroComponent implements OnInit {

  @Input() modal: any;
  @Input() equipo: Equipo;
  jugador: Jugador = new Jugador();

  constructor(private jugadorService: JugadorService) {
  }

  ngOnInit() {
    this.jugador.genero = this.equipo.torneo.genero;
  }

  saveJugador() {
    this.jugadorService.insertarJugador(this.equipo.codigo.toString(), this.jugador).subscribe();
    this.modal.close('Close click');
  }

}
