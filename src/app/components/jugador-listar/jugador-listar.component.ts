import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { EquipoService } from 'src/app/services/equipo.service';
import { Equipo } from 'src/app/model/equipo';
import { Jugador } from 'src/app/model/jugador';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-jugador-listar',
  templateUrl: './jugador-listar.component.html',
  styleUrls: ['./jugador-listar.component.css']
})
export class JugadorListarComponent implements OnInit {

  equipo: Equipo = new Equipo();
  jugadores: Jugador[] = [];

  constructor(private route: ActivatedRoute, private modalService: NgbModal,
     private equipoService: EquipoService, private router: Router) {

    this.equipoService.findEquipoById(this.route.snapshot.paramMap.get('idEquipo')).subscribe(
      equipo => {
        this.equipo = equipo;
        this.jugadores = equipo.jugadores;
      }
    );
   }

  ngOnInit() {
  }

  openModalCrear(content: any) {
    this.modalService.open(content, { size: 'lg' });
  }
}
