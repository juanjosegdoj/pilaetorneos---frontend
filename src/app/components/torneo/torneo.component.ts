import { Component, OnInit, ViewEncapsulation} from '@angular/core';
import { Torneo } from 'src/app/model/torneo';
import { TorneoService } from 'src/app/services/torneo.service';
import { Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-torneo',
  templateUrl: './torneo.component.html',
  styleUrls: ['./torneo.component.css'],
  encapsulation: ViewEncapsulation.None,
})
export class TorneoComponent implements OnInit {

  torneos: Torneo[] = [];
  torneoSeleccionado: Torneo = new Torneo();

  constructor(private torneoService: TorneoService, private router: Router, private modalService: NgbModal) { }

  ngOnInit() {
    this.torneoService.listAll().subscribe(
      torneos => {
        this.torneos = torneos;
      }
    );
  }

  openModalEditar(content: any) {
    this.modalService.open(content, { size: 'lg' });
  }

  openModalCrear(content: any) {
    this.modalService.open(content, { size: 'lg' });
  }

  selectTorneo(torneo: Torneo) {
    this.torneoSeleccionado = torneo;
  }
}
