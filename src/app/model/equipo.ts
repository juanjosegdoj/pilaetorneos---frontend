import { Jugador } from './jugador';
import { Torneo } from './torneo';

export class Equipo {
    codigo: number;
    nombre: string;
    jugadores: Jugador[];
    torneo: Torneo;
}
