export class AppSettings {
    public static API_ENDPOINT = 'http://localhost:8081/';
    public static TORNEO_ENDPOINT = 'torneos';
    public static EQUIPO_ENDPOINT = 'equipos';
    public static DEPORTE_ENDPOINT = 'deportes';
    public static GENERO_ENDPOINT = 'generos';
    public static JUGADOR_ENDPOINT = 'jugadores';
}
