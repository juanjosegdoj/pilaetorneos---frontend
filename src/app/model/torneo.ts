import { Genero } from './genero';
import { Deporte } from './deporte';

export class Torneo {

    codigo: number;
    nombre: string;
    descripcion: string;
    deporte: Deporte;
    genero: Genero;
    fechaInicio: Date;
    limitefechaNacimiento: Date;
    cantMinJugadoresPorEquipo: number;
    cantMaxJugadoresPorEquipo: number;
}
