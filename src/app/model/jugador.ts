import { Genero } from './genero';

export class Jugador {
    codigo: number;
    identificacion: string;
    nombre: string;
    apellido: string;
    genero: Genero;
    fechaNacimiento: Date;
}
