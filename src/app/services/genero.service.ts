import { Injectable } from '@angular/core';
import { AppSettings } from '../model/app-settings';
import { HttpClient } from '@angular/common/http';
import { Genero } from '../model/genero';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class GeneroService {

  private END_POINTBASE = AppSettings.API_ENDPOINT.concat(AppSettings.GENERO_ENDPOINT);
  constructor( private http: HttpClient ) {}

  listAll(): Observable <Genero[]> {
    return this.http.get<Genero[]>(this.END_POINTBASE.concat('/listar'));
  }
}
