import { Injectable } from '@angular/core';
import { AppSettings } from '../model/app-settings';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Equipo } from '../model/equipo';


@Injectable({
  providedIn: 'root'
})
export class EquipoService {
  private headerJson = new HttpHeaders({'Content-Type': 'application/json'});
  private END_POINTBASE = AppSettings.API_ENDPOINT.concat(AppSettings.EQUIPO_ENDPOINT);

  constructor(private http: HttpClient) { }

  findEquiposByTorneo(codTorneo: string): Observable<Equipo[]> {
    return this.http.get<Equipo[]>(this.END_POINTBASE.concat('?codTorneo=').concat(codTorneo));
  }

  findEquipoById(codTorneo: string): Observable<Equipo> {
    return this.http.get<Equipo>(this.END_POINTBASE.concat('/').concat(codTorneo));
  }

  insertarEquipoId(codTorneo: string, object: Equipo) {
    return this.http.post<Equipo>(this.END_POINTBASE.concat('/insertar/?codTorneo=').concat(codTorneo), object, {headers: this.headerJson});
  }
}
