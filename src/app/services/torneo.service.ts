import { Injectable } from '@angular/core';
import { AppSettings } from '../model/app-settings';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Torneo } from '../model/torneo';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class TorneoService {

  private headerJson = new HttpHeaders({'Content-Type': 'application/json'});
  private END_POINTBASE = AppSettings.API_ENDPOINT.concat(AppSettings.TORNEO_ENDPOINT);

  constructor( private http: HttpClient ) {}

  createTorneo(object: Torneo): Observable<Torneo> {
    return this.http.post<Torneo>(
      this.END_POINTBASE.concat('/insertar'), object, {headers: this.headerJson}
      );
  }

  editarTorneo(object: Torneo) {
    return this.http.put<Torneo>(
      this.END_POINTBASE.concat('/editar'), object, {headers: this.headerJson}
      );
  }

  deleteTorneo(codTorneo: string) {
    return this.http.delete<Torneo>(
      this.END_POINTBASE.concat('/'.concat(codTorneo).concat('/eliminar'))
      );
  }

  listAll(): Observable <Torneo[]> {
    return this.http.get<Torneo[]>(this.END_POINTBASE.concat('/listar'));
  }

  findById(id: string): Observable<Torneo> {
    return this.http.get<Torneo>(this.END_POINTBASE.concat('/').concat(id));
  }
}
