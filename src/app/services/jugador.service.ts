import { Injectable } from '@angular/core';
import { AppSettings } from '../model/app-settings';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Jugador } from '../model/jugador';

@Injectable({
  providedIn: 'root'
})
export class JugadorService {

  private headerJson = new HttpHeaders({'Content-Type': 'application/json'});
  private END_POINTBASE = AppSettings.API_ENDPOINT.concat(AppSettings.JUGADOR_ENDPOINT);

  constructor(private http: HttpClient ) { }

  insertarJugador(codEquipo: string, object: Jugador) {
    return this.http.post<Jugador>(this.END_POINTBASE.concat('/insertar/?codEquipo=')
    .concat(codEquipo), object, {headers: this.headerJson});
  }
}
