import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AppSettings } from '../model/app-settings';
import { Deporte } from '../model/deporte';
import { Observable } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class DeporteService {

  private END_POINTBASE = AppSettings.API_ENDPOINT.concat(AppSettings.DEPORTE_ENDPOINT);

  constructor( private http: HttpClient ) {}

  listAll(): Observable <Deporte[]> {
    return this.http.get<Deporte[]>(this.END_POINTBASE.concat('/listar'));
  }
}
