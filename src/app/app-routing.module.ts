import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { InicioComponent } from './components/inicio/inicio.component';
import { DeporteComponent } from './components/deporte/deporte.component';
import { TorneoComponent } from './components/torneo/torneo.component';
import { PanelTorneoComponent } from './components/panel-torneo/panel-torneo.component';
import { TorneoConfiguracionComponent } from './components/torneo-configuracion/torneo-configuracion.component';
import { EquipoListarComponent } from './components/equipo-listar/equipo-listar.component';
import { JugadorListarComponent } from './components/jugador-listar/jugador-listar.component';

const routes: Routes = [
  {path: '', component: InicioComponent},
  {path: 'deportes', component: DeporteComponent},
  {path: 'torneos', component: TorneoComponent},
  {path: 'torneo', component: PanelTorneoComponent,
    children: [
      { path: ':id/configuracion', component: TorneoConfiguracionComponent},
      { path: ':id/equipos', component: EquipoListarComponent},
      { path: ':id/clasificacion', component: EquipoListarComponent},
      { path: ':id/calendario', component: EquipoListarComponent},
      { path: ':idTorneo/equipo/:idEquipo', component: JugadorListarComponent},
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
