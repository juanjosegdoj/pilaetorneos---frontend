import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HttpClientModule } from '@angular/common/http';
import { FooterComponent } from './components/footer/footer.component';
import { HeaderComponent } from './components/header/header.component';
import { InicioComponent } from './components/inicio/inicio.component';
import { DeporteComponent } from './components/deporte/deporte.component';
import { TorneoComponent } from './components/torneo/torneo.component';
import { TorneoRegistroComponent } from './components/torneo-registro/torneo-registro.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { TorneoEditarComponent } from './components/torneo-editar/torneo-editar.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { TorneoConfiguracionComponent } from './components/torneo-configuracion/torneo-configuracion.component';
import { PanelTorneoComponent } from './components/panel-torneo/panel-torneo.component';
import {NoopAnimationsModule} from '@angular/platform-browser/animations';
import {MatButtonModule, MatCheckboxModule, MatTabsModule, MatTableModule,
  MatMenuModule, MatSelectModule, MatGridListModule } from '@angular/material';
import { EquipoListarComponent } from './components/equipo-listar/equipo-listar.component';
import { EquipoRegistrarComponent } from './components/equipo-registrar/equipo-registrar.component';
import { JugadorListarComponent } from './components/jugador-listar/jugador-listar.component';
import { JugadorRegistroComponent } from './components/jugador-registro/jugador-registro.component';


@NgModule({
  declarations: [
    AppComponent,
    FooterComponent,
    HeaderComponent,
    InicioComponent,
    DeporteComponent,
    TorneoComponent,
    TorneoRegistroComponent,
    TorneoEditarComponent,
    PanelTorneoComponent,
    TorneoConfiguracionComponent,
    EquipoListarComponent,
    EquipoRegistrarComponent,
    JugadorListarComponent,
    JugadorRegistroComponent
    ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    BrowserAnimationsModule,
    NgbModule,
    NoopAnimationsModule,
    MatButtonModule,
    MatCheckboxModule,
    MatTabsModule,
    MatMenuModule,
    MatSelectModule,
    MatGridListModule,
    MatTableModule,
    MatCheckboxModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
